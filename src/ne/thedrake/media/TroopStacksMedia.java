package ne.thedrake.media;

import ne.thedrake.game.BasicTroopStacks;

public interface TroopStacksMedia<T> {

    public T putBasicTroopStacks(BasicTroopStacks stacks);
}

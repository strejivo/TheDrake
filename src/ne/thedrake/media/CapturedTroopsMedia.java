package ne.thedrake.media;

import ne.thedrake.game.CapturedTroops;

public interface CapturedTroopsMedia<T> {

    public T putCapturedTroops(CapturedTroops captured);
}

package ne.thedrake.media.plaintext;

import java.io.OutputStream;
import java.io.PrintWriter;

import ne.thedrake.game.BasicTroopStacks;
import ne.thedrake.game.PlayingSide;
import ne.thedrake.game.TroopInfo;
import ne.thedrake.media.PrintMedia;
import ne.thedrake.media.TroopStacksMedia;

public class TroopStacksPlainTextMedia extends PrintMedia implements TroopStacksMedia<Void>{

    public TroopStacksPlainTextMedia(OutputStream stream){
        super(stream);
    }
    
    @Override
    public Void putBasicTroopStacks(BasicTroopStacks stacks) {

        PrintWriter w = writer();
        
        w.print(PlayingSide.BLUE.toString()+" stack:");
        for(TroopInfo troop : stacks.troops(PlayingSide.BLUE))
            w.print(" "+troop.name());
        w.println();
                
        w.print(PlayingSide.ORANGE.toString()+" stack:");
        for(TroopInfo troop : stacks.troops(PlayingSide.ORANGE))
            w.print(" "+troop.name());
        w.println();
        
        return null;
        
    }
    
}

package ne.thedrake.media.plaintext;

import java.io.OutputStream;
import java.io.PrintWriter;

import ne.thedrake.game.BasicTroopStacks;
import ne.thedrake.game.MiddleGameState;
import ne.thedrake.game.PlacingGuardsGameState;
import ne.thedrake.game.PlacingLeadersGameState;
import ne.thedrake.game.PlayingSide;
import ne.thedrake.game.TilePosition;
import ne.thedrake.game.VictoryGameState;
import ne.thedrake.media.GameStateMedia;
import ne.thedrake.media.PrintMedia;

public class GameStatePlainTextMedia extends PrintMedia implements GameStateMedia<Void>{

    private final TroopStacksPlainTextMedia troopStacksMedia;
    private final LeadersPlainTextMedia leadersMedia;
    private final BoardPlainTextMedia boardMedia;
    
    public GameStatePlainTextMedia(OutputStream stream){
        super(stream);
        this.troopStacksMedia = new TroopStacksPlainTextMedia(stream);
        this.leadersMedia = new LeadersPlainTextMedia(stream);
        this.boardMedia = new BoardPlainTextMedia(stream);
    }
    
    @Override
    public Void putPlacingLeadersGameState(PlacingLeadersGameState state) {
        PrintWriter w = writer();
        w.println("LEADERS");
        w.println("0");
        w.println(state.sideOnTurn().toString());
        state.troopStacks().putToMedia(troopStacksMedia);
        state.leaders().putToMedia(leadersMedia);
        state.board().putToMedia(boardMedia);
        return null;
    }

    @Override
    public Void putPlacingGuardsGameState(PlacingGuardsGameState state) {
        
        PrintWriter w = writer();
        w.println("GUARDS");
        w.println(state.guardsCount());
        w.println(state.sideOnTurn().toString());
        state.troopStacks().putToMedia(troopStacksMedia);
        state.leaders().putToMedia(leadersMedia);
        state.board().putToMedia(boardMedia);

        return null;
        
    }

    @Override
    public Void putMiddleGameState(MiddleGameState state) {
        PrintWriter w = writer();
        w.println("MIDDLE");
        w.println("4");
        w.println(state.sideOnTurn().toString());
        state.troopStacks().putToMedia(troopStacksMedia);
        state.leaders().putToMedia(leadersMedia);
        state.board().putToMedia(boardMedia);
        return null;
    }

    @Override
    public Void putFinishedGameState(VictoryGameState state) {
        PrintWriter w = writer();
        w.println("VICTORY");
        w.println("4");
        w.println(state.sideOnTurn().toString());
        state.troopStacks().putToMedia(troopStacksMedia);
        w.print("OL ");
        w.flush();
        if(state.leaders().isPlaced(PlayingSide.BLUE))
            w.println(state.leaders().position(PlayingSide.BLUE)+" X");
        else{
            w.println("X "+state.leaders().position(PlayingSide.ORANGE));
        }
        state.board().putToMedia(boardMedia);
        return null;
    }
    
}

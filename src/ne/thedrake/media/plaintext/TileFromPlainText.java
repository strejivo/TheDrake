package ne.thedrake.media.plaintext;

import java.io.BufferedReader;
import java.io.IOException;

import ne.thedrake.game.EmptyTile;
import ne.thedrake.game.PlayingSide;
import ne.thedrake.game.TheDrakeSetup;
import ne.thedrake.game.Tile;
import ne.thedrake.game.TilePosition;
import ne.thedrake.game.Troop;
import ne.thedrake.game.TroopFace;
import ne.thedrake.game.TroopInfo;
import ne.thedrake.game.TroopTile;

public class TileFromPlainText {

    private final TheDrakeSetup setup;
    private final BufferedReader reader;

    public TileFromPlainText(TheDrakeSetup setup, BufferedReader reader) {
        this.setup = setup;
        this.reader = reader;
    }

    public Tile readTile(TilePosition position) throws IOException {
        String line = reader.readLine();

        if ("empty".equals(line)) {
            return new EmptyTile(position);
        }

        String[] fields = line.split(" ");
        TroopInfo info = setup.infoByName(fields[0]);
        PlayingSide side = PlayingSide.valueOf(fields[1]);
        TroopFace face = TroopFace.valueOf(fields[2]);
        Troop troop = new Troop(info, side, face);
        return new TroopTile(position, troop);
    }
}

package ne.thedrake.media.plaintext;

import java.io.OutputStream;
import java.io.PrintWriter;

import ne.thedrake.game.EmptyTile;
import ne.thedrake.game.TroopTile;
import ne.thedrake.media.PrintMedia;
import ne.thedrake.media.TileMedia;

public class TilePlainTextMedia extends PrintMedia implements TileMedia<Void>{
    
    public TilePlainTextMedia(OutputStream stream){
        super(stream);
    }

    @Override
    public Void putTroopTile(TroopTile tile) {
        PrintWriter w = writer();
        w.println(tile.troop().info().name()+" "+tile.troop().side().toString()+" "+tile.troop().face().toString());
        return null;
    }

    @Override
    public Void putEmptyTile(EmptyTile tile) {
        PrintWriter w = writer();
        w.println("empty");
        return null;
    }
    
}

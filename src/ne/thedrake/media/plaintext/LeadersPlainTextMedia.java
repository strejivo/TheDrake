package ne.thedrake.media.plaintext;

import java.io.OutputStream;
import java.io.PrintWriter;

import ne.thedrake.game.BothLeadersPlaced;
import ne.thedrake.game.NoLeadersPlaced;
import ne.thedrake.game.OneLeaderPlaced;
import ne.thedrake.game.PlayingSide;
import ne.thedrake.media.LeadersMedia;
import ne.thedrake.media.PrintMedia;

public class LeadersPlainTextMedia extends PrintMedia implements LeadersMedia<Void>{

    public LeadersPlainTextMedia(OutputStream stream){
        super(stream);
    }
    
    @Override
    public Void putNoLeadersPlaced(NoLeadersPlaced leaders) {
        PrintWriter w = writer();
        w.println("NL");
        return null;
    }

    @Override
    public Void putOneLeaderPlaced(OneLeaderPlaced leaders) {
        PrintWriter w = writer();
        w.print("OL ");
        if(leaders.isPlaced(PlayingSide.BLUE)){
            w.print(leaders.position(PlayingSide.BLUE).column());
            w.flush();
            w.println(leaders.position(PlayingSide.BLUE).row());
        }
        else
            w.println(leaders.position(PlayingSide.ORANGE).column()+leaders.position(PlayingSide.ORANGE).row());
        return null;
    }

    @Override
    public Void putBothLeadersPlaced(BothLeadersPlaced leaders) {
        PrintWriter w = writer();
        w.println("BL "+leaders.position(PlayingSide.BLUE)+" "+leaders.position(PlayingSide.ORANGE));
        return null;
    }
    
}

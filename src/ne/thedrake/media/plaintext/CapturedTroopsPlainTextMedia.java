package ne.thedrake.media.plaintext;

import java.io.OutputStream;
import java.io.PrintWriter;

import ne.thedrake.game.CapturedTroops;
import ne.thedrake.game.PlayingSide;
import ne.thedrake.game.TroopInfo;
import ne.thedrake.media.BoardMedia;
import ne.thedrake.media.CapturedTroopsMedia;
import ne.thedrake.media.PrintMedia;

public class CapturedTroopsPlainTextMedia extends PrintMedia implements CapturedTroopsMedia<Void>{

    public CapturedTroopsPlainTextMedia(OutputStream stream){
        super(stream);
    }
    
    @Override
    public Void putCapturedTroops(CapturedTroops captured) {
    
        PrintWriter w = writer();
        
        w.println("Captured "+PlayingSide.BLUE.toString()+": "+captured.troops(PlayingSide.BLUE).size());
        
        for(TroopInfo troop : captured.troops(PlayingSide.BLUE))
            w.println(troop.name());
        
        if(captured.troops(PlayingSide.ORANGE).size() > 0)
            w.println("Captured "+PlayingSide.ORANGE.toString()+": "+captured.troops(PlayingSide.ORANGE).size());
        else{
            w.print("Captured "+PlayingSide.ORANGE.toString()+": "+captured.troops(PlayingSide.ORANGE).size());
            w.flush();
        }
        
        int cnt = 0;
        for(TroopInfo troop : captured.troops(PlayingSide.ORANGE)){
            if(cnt == captured.troops(PlayingSide.ORANGE).size()-1){
                w.print(troop.name());
                w.flush();
            }
            else{
                w.println(troop.name());
            }
            cnt++;
        }
        
        return null;
        
    }
    
}

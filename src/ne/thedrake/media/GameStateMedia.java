package ne.thedrake.media;

import ne.thedrake.game.MiddleGameState;
import ne.thedrake.game.PlacingGuardsGameState;
import ne.thedrake.game.PlacingLeadersGameState;
import ne.thedrake.game.VictoryGameState;

public interface GameStateMedia<T> {

    public T putPlacingLeadersGameState(PlacingLeadersGameState state);

    public T putPlacingGuardsGameState(PlacingGuardsGameState state);

    public T putMiddleGameState(MiddleGameState state);

    public T putFinishedGameState(VictoryGameState state);
}

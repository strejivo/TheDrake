package ne.thedrake.media;

import ne.thedrake.game.Board;

public interface BoardMedia<T> {

    public T putBoard(Board board);
}

package ne.thedrake.media;

import ne.thedrake.game.EmptyTile;
import ne.thedrake.game.TroopTile;

public interface TileMedia<T> {

    public T putTroopTile(TroopTile tile);

    public T putEmptyTile(EmptyTile tile);
}

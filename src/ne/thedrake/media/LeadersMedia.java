package ne.thedrake.media;

import ne.thedrake.game.BothLeadersPlaced;
import ne.thedrake.game.NoLeadersPlaced;
import ne.thedrake.game.OneLeaderPlaced;

public interface LeadersMedia<T> {

    public T putNoLeadersPlaced(NoLeadersPlaced leaders);

    public T putOneLeaderPlaced(OneLeaderPlaced leaders);

    public T putBothLeadersPlaced(BothLeadersPlaced leaders);
}

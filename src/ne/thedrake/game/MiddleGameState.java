package ne.thedrake.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ne.thedrake.media.GameStateMedia;

public class MiddleGameState extends BaseGameState {

    public MiddleGameState(
            Board board,
            TroopStacks troopStacks,
            BothLeadersPlaced leaders,
            PlayingSide sideOnTurn) {
        super(
                board,
                troopStacks,
                leaders,
                sideOnTurn);
    }

    @Override
    public BothLeadersPlaced leaders() {
        return (BothLeadersPlaced) super.leaders();
    }

    @Override
    public List<Move> allMoves() {
        List<Move> result = new ArrayList<>(stackMoves());
        for (Tile tile : board()) {
            result.addAll(boardMoves(tile.position()));
        }
        return result;
    }

    @Override
    public List<Move> boardMoves(TilePosition position) {
        if (!board().contains(position)) {
            return Collections.EMPTY_LIST;
        }
        List<Move> result = new ArrayList<>();
        if (board().tileAt(position).hasTroop() 
            && board().tileAt(position).troop().side() == sideOnTurn()
           ) {
            for (BoardChange change : board().tileAt(position).troop().changesFrom(position, board())) {
                result.add(new BoardMove(this, change));
            }
        }
        return result;
    }

    @Override
    public List<Move> stackMoves() {
        List<Move> result = new ArrayList<>();
        Troop troop = troopStacks().peek(sideOnTurn());
        for (Tile tile : board()) {
            if (canPlaceFromStack(tile.position())) {
                result.add(new PlaceFromStack(this, tile.position()));
            }
        }
        return result;
    }

    @Override
    public boolean isVictory() {
        return false;
    }

    public boolean canPlaceFromStack(TilePosition target) {
        Troop troop = troopStacks().peek(sideOnTurn());
        if (troop == null) {
            return false;
        }
        if (!board().canPlaceTo(troop, target)) {
            return false;
        }
        boolean hasNeigbour = false;
        hasNeigbour |= tryNeighbour(target, 0, 1);
        hasNeigbour |= tryNeighbour(target, 0, -1);
        hasNeigbour |= tryNeighbour(target, 1, 0);
        hasNeigbour |= tryNeighbour(target, -1, 0);

        return hasNeigbour;
    }

    public MiddleGameState placeFromStack(TilePosition target) {
        Troop troop = troopStacks().peek(sideOnTurn());
        return new MiddleGameState(
                board().withTiles(
                        new TroopTile(target, troop)),
                troopStacks().pop(sideOnTurn()),
                leaders(),
                sideOnTurn().opposite());
    }

    private boolean tryNeighbour(TilePosition origin, int xStep, int yStep) {
        return board().contains(origin.step(xStep, yStep))
                && board().tileAt(origin.step(xStep, yStep)).hasTroop()
                && board().tileAt(origin.step(xStep, yStep)).troop().side() == sideOnTurn();
    }

    @Override
    public <T> T putToMedia(GameStateMedia<T> media) {
        return media.putMiddleGameState(this);
    }
}

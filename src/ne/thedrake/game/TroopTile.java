package ne.thedrake.game;

import ne.thedrake.media.TileMedia;

public class TroopTile extends Tile {

    public TroopTile(TilePosition position, Troop troop) {
        super(position);
        this.troop = troop;
    }

    @Override
    public boolean acceptsTroop(Troop troop) {
        return false;
    }

    @Override
    public boolean hasTroop() {
        return true;
    }

    @Override
    public Troop troop() {
        return troop;
    }

    final private Troop troop;

    @Override
    public <T> T putToMedia(TileMedia<T> media) {
        return media.putTroopTile(this);
    }

}

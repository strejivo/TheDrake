package ne.thedrake.game;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import ne.thedrake.media.BoardMedia;

public class Board implements Iterable<Tile> {

    // Primární konstruktor
    public Board(int dimension, CapturedTroops captured, Tile... tiles) {
        this.dimension = dimension;
        this.tiles = new Tile[dimension][dimension];
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                this.tiles[i][j] = new EmptyTile(new TilePosition(i, j));
            }
        }
        for (int i = 0; i < tiles.length; i++) {
            TilePosition tmp = tiles[i].position();
            this.tiles[tmp.i][tmp.j] = tiles[i];
        }
        this.captured = captured;
    }   

    // Konstruktor. Vytvoří čtvercovou hrací desku zadaného rozměru se specefikovanými dlaždicemi.
    // Všechny ostatní dlažice se berou jako prázdné.
    public Board(int dimension, Tile... tiles) {
        this(dimension, new CapturedTroops(), tiles);
    }

    // Rozměr hrací desky
    public int dimension() {
        return dimension;
    }

    // Vrací dlaždici na zvolené pozici. Pokud je pozice mimo desku, vyhazuje IllegalArgumentException
    public Tile tileAt(TilePosition position) {
        if (!contains(position)) {
            throw new IllegalArgumentException("Pozice mimo desku.");
        }
        return tiles[position.i][position.j];
    }

    // Ověřuje, že pozice se nachází na hrací desce
    public boolean contains(TilePosition... positions) {
        for (int i = 0; i < positions.length; i++) {
            TilePosition tmp = positions[i];
            if (tmp.i >= dimension || tmp.i < 0 || tmp.j >= dimension || tmp.j < 0) {
                return false;
            }
        }
        return true;
    }

    // Vytváří novou hrací desku s novými dlaždicemi
    public Board withTiles(Tile... tiles) {
        Board ret = new Board(dimension);
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                ret.tiles[i][j] = this.tiles[i][j];
            }
        }
        for (int i = 0; i < tiles.length; i++) {
            TilePosition tmp = tiles[i].position();
            ret.tiles[tmp.i][tmp.j] = tiles[i];
        }
        ret.captured = new CapturedTroops(captured);
        return ret;
    }

    // Vytváří novou hrací desku s novými dlaždicemi a novou zajatou jednotkou pro konkrétního hráče
    public Board withCaptureAndTiles(TroopInfo info, PlayingSide side, Tile... tiles) {
        Board ret = this.withTiles(tiles);
        ret.captured = ret.captured.withTroop(side, info);
        return ret;
    }

    // Vrací zajaté jednotky
    public CapturedTroops captured() {
        return captured;
    }

    // Stojí na pozici origin jednotka?
    public boolean canTakeFrom(TilePosition origin) {
        return contains(origin) && tileAt(origin).hasTroop();
    }

    /*
     * Lze na danou pozici postavit zadanou jednotku? Zde se řeší pouze
     * jednotky na hrací ploše, nikoliv zásobník, takže se v podstatě
     * pouze ptám, zda dlaždice na pozici target přijme danou jednotku.
     */
    public boolean canPlaceTo(Troop troop, TilePosition target) {
        return contains(target) && tileAt(target).acceptsTroop(troop);
    }

    // Může zadaná jednotka zajmout na pozici target soupeřovu jednotku?
    public boolean canCaptureOn(Troop troop, TilePosition target) {
        return contains(target) && (canTakeFrom(target)
                && tileAt(target).troop().side() != troop.side());
    }

    /*
     * Stojí na políčku origin jednotka, která může udělat krok na pozici target
     * bez toho, aby tam zajala soupeřovu jednotku?
     */
    public boolean canStepOnly(TilePosition origin, TilePosition target) {
        return contains(origin) && contains(target) && (canTakeFrom(origin) && !canTakeFrom(target));
    }

    /*
     * Stojí na políčku origin jednotka, která může zůstat na pozici origin
     * a zajmout soupeřovu jednotku na pozici target?
     */
    public boolean canCaptureOnly(TilePosition origin, TilePosition target) {
        return (contains(origin) && contains(target) && canTakeFrom(origin) && (canCaptureOn(tileAt(origin).troop(), target)));
    }

    /*
     * Stojí na pozici origin jednotka, která může udělat krok na pozici target
     * a zajmout tam soupeřovu jednotku?
     */
    public boolean canStepAndCapture(TilePosition origin, TilePosition target) {
        if (contains(origin) && contains(target) && canTakeFrom(origin) && canCaptureOn(tileAt(origin).troop(), target)) {
            EmptyTile tmp = new EmptyTile(target);
            Board tmpBoard = withTiles(tmp);
            if (tmpBoard.canPlaceTo(tileAt(origin).troop(), target)) {
                return true;
            }
        }
        return false;
    }

    /*
     * Nová hrací deska, ve které jednotka na pozici origin se přesunula
     * na pozici target bez toho, aby zajala soupeřovu jednotku.
     */
    public Board stepOnly(TilePosition origin, TilePosition target) {

        Troop toMove = tileAt(origin).troop();
        TroopTile filledTile = new TroopTile(target, toMove.flipped());
        EmptyTile emptiedTile = new EmptyTile(origin);

        return withTiles(filledTile, emptiedTile);

    }

    /*
     * Nová hrací deska, ve které jednotka na pozici origin se přesunula
     * na pozici target, kde zajala soupeřovu jednotku.
     */
    public Board stepAndCapture(TilePosition origin, TilePosition target) {

        Troop attacker = tileAt(origin).troop();
        Troop targetTroop = tileAt(target).troop();

        return withCaptureAndTiles(
                targetTroop.info(),
                targetTroop.side(),
                new EmptyTile(origin),
                new TroopTile(target, attacker.flipped())
        );

    }

    /*
     * Nová hrací deska, ve které jednotka zůstává stát na pozici origin
     * a zajme soupeřovu jednotku na pozici target.
     */
    public Board captureOnly(TilePosition origin, TilePosition target) {

        Troop attacker = tileAt(origin).troop();
        Troop targetTroop = tileAt(target).troop();

        return withCaptureAndTiles(
                targetTroop.info(),
                targetTroop.side(),
                new EmptyTile(target),
                new TroopTile(origin, attacker.flipped())
        );

    }
    
    public <T> T putToMedia(BoardMedia<T> media){
        return media.putBoard(this);
    }

    private final int dimension;
    private final Tile[][] tiles;
    CapturedTroops captured;

    @Override
    public Iterator<Tile> iterator() {
        return new Iterator<Tile>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < dimension * dimension;
            }

            @Override
            public Tile next() {
                Tile next = tiles[index % dimension][index / dimension];
                index++;
                return next;

            }
        };
    }
}

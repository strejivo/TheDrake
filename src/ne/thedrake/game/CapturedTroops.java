package ne.thedrake.game;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class CapturedTroops {

    // Konstruktor vytvářející prázdné seznamy
    public CapturedTroops() {
        orangeCapturedTroops = new ArrayList<TroopInfo>();
        blueCapturedTroops = new ArrayList<TroopInfo>();
    }

    public CapturedTroops(CapturedTroops old) {
        orangeCapturedTroops = new ArrayList<TroopInfo>();
        blueCapturedTroops = new ArrayList<TroopInfo>();
        for (TroopInfo x : old.blueCapturedTroops) {
            this.blueCapturedTroops.add(x);
        }
        for (TroopInfo x : old.orangeCapturedTroops) {
            this.orangeCapturedTroops.add(x);
        }
    }
    
    public CapturedTroops(List<TroopInfo> blueTroops, List<TroopInfo> orangeTroops){
        this.orangeCapturedTroops = new ArrayList<>();
        this.blueCapturedTroops = new ArrayList<>();
        for(TroopInfo item : orangeTroops){
            this.orangeCapturedTroops.add(item);
        }
        for(TroopInfo item : blueTroops){
            this.blueCapturedTroops.add(item);
        }
    }

    // Vrací seznam zajatých jednotek pro daného hráče
    public List<TroopInfo> troops(PlayingSide side) {
        if (side == PlayingSide.ORANGE) {
            return Collections.unmodifiableList(orangeCapturedTroops);
        } else {
            return Collections.unmodifiableList(blueCapturedTroops);
        }
    }

    // Přidává nově zajatou jednotku na začátek seznamu zajatých jednotek daného hráče.
    public CapturedTroops withTroop(PlayingSide side, TroopInfo info) {
        CapturedTroops tmp = new CapturedTroops();
        for (int i = 0; i < orangeCapturedTroops.size(); i++) {
            tmp.orangeCapturedTroops.add(this.orangeCapturedTroops.get(i));
        }
        for (int i = 0; i < blueCapturedTroops.size(); i++) {
            tmp.blueCapturedTroops.add(this.blueCapturedTroops.get(i));
        }
        if (side == PlayingSide.ORANGE) {
            tmp.orangeCapturedTroops.add(0, info);
        } else {
            tmp.blueCapturedTroops.add(0, info);
        }
        return tmp;
    }

    List<TroopInfo> orangeCapturedTroops;
    List<TroopInfo> blueCapturedTroops;

}

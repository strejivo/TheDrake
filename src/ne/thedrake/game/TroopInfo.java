package ne.thedrake.game;

import java.util.Collections;
import java.util.List;

public class TroopInfo {

    // Konstruktor
    public TroopInfo(String name, Offset2D frontPivot, Offset2D backPivot, List<TroopAction> frontActions, List<TroopAction> backActions) {
        this.name = name;
        this.frontPivot = frontPivot;
        this.backPivot = backPivot;
        this.frontActions = frontActions;
        this.backActions = backActions;
    }

    public TroopInfo(String name, List<TroopAction> frontActions, List<TroopAction> backActions) {
        this(name, new Offset2D(1, 1), new Offset2D(1, 1), frontActions, backActions);
    }

    public TroopInfo(String name, List<TroopAction> actions) {
        this(name, new Offset2D(1, 1), new Offset2D(1, 1), actions, actions);
    }

    public TroopInfo(String name, Offset2D frontPivot, Offset2D backPivot) {
        this(name, frontPivot, backPivot, Collections.EMPTY_LIST, Collections.EMPTY_LIST);
    }

    // Konstruktor, který nastaví lícový i rubový pivot na stejnou hodnotu
    public TroopInfo(String name, Offset2D pivot) {
        this(name, pivot, pivot, Collections.EMPTY_LIST, Collections.EMPTY_LIST);
    }

    // Konstruktor, který nastaví lícový i rubový pivot na hodnotu [1, 1]
    public TroopInfo(String name) {
        this(name, new Offset2D(1, 1), new Offset2D(1, 1), Collections.EMPTY_LIST, Collections.EMPTY_LIST);
    }

    // Vrací jméno
    public String name() {
        return this.name;
    }

    // Vrací pivot na zadané straně jednotky
    public Offset2D pivot(TroopFace face) {
        return (face == TroopFace.FRONT ? frontPivot : backPivot);
    }

    public List<TroopAction> actions(TroopFace face) {
        return (face == TroopFace.FRONT ? frontActions : backActions);
    }

    private final String name;
    private final Offset2D frontPivot, backPivot;
    private final List<TroopAction> frontActions, backActions;
}

package ne.thedrake.game;

public class Offset2D {

    // Konstruktor
    public Offset2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Zjištuje, zda se tento offset rovná jinému offsetu
    public boolean equalsTo(int x, int y) {
        return (this.x == x && this.y == y);
    }

    // Vrací nový offset s obrácenou y souřadnicí
    public Offset2D yFlipped() {
        return new Offset2D(x, -y);
    }

    public final int x, y;

}

package ne.thedrake.game;

import java.util.ArrayList;
import java.util.List;

public class Troop {

    // Konstruktor
    public Troop(TroopInfo info, PlayingSide side, TroopFace face) {
        this.info = info;
        this.side = side;
        this.face = face;
    }

    // Vytvoří jednotku lícem nahoru
    public Troop(TroopInfo info, PlayingSide side) {
        this(info, side, TroopFace.FRONT);
    }

    // Info o jednotce
    public TroopInfo info() {
        return info;
    }

    // Barva, za kterou jednotka hraje
    public PlayingSide side() {
        return side;
    }

    // Kterou stranou je jednotka otočena nahoru
    public TroopFace face() {
        return face;
    }

    // Pivot té strany, kterou je zrovna jednotka otočena nahoru
    public Offset2D pivot() {
        return info.pivot(face);
    }

    // Vytvoří jednotku, která má stejné vlastnosti jako tato, jen je otočena druhou stranou nahoru.
    public Troop flipped() {
        TroopFace flippedFace = TroopFace.FRONT;
        if (face == TroopFace.FRONT) {
            flippedFace = TroopFace.BACK;
        }
        return new Troop(info, side, flippedFace);
    }

    // Všechny změny desky, které může jednotka provést na desce board, pokud stojí na pozici pos.
    public List<BoardChange> changesFrom(TilePosition pos, Board board) {
        List<BoardChange> result = new ArrayList<BoardChange>();
        if (!board.contains(pos)) {
            return result;
        }
        for (TroopAction action : info.actions(face)) {
            result.addAll(action.changesFrom(pos, side, board));
        }
        return result;
    }

    private final TroopInfo info;
    private final PlayingSide side;
    private final TroopFace face;

}

package ne.thedrake.game;

public enum PlayingSide {

    ORANGE, BLUE {
        @Override
        public PlayingSide opposite() {
            return ORANGE;
        }
    };

    public PlayingSide opposite() {
        return BLUE;
    }

}

package ne.thedrake.game;

import ne.thedrake.media.TileMedia;

public class EmptyTile extends Tile {

    public EmptyTile(TilePosition position) {
        super(position);
    }

    @Override
    public boolean acceptsTroop(Troop troop) {
        return true;
    }

    @Override
    public boolean hasTroop() {
        return false;
    }

    @Override
    public Troop troop() {
        throw new UnsupportedOperationException("There's no troop.");
    }

    @Override
    public <T> T putToMedia(TileMedia<T> media) {
        return media.putEmptyTile(this);
    }

}

package ne.thedrake.game;

import java.util.List;

public interface TheDrakeSetup {

    // Vrátí info jednotky podle jména jednotky
    public TroopInfo infoByName(String name);

    public List<TroopInfo> troops();
}
